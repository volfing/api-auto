<?php

use yii\db\Migration;

/**
 * Class m190624_164929_new_api_access_table
 */
class m190624_164929_new_api_access_table extends Migration
{
    private $tableName = "api_access";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'api_key' => $this->string(100)->notNull(),
            'title' => $this->string(100)->notNull(),
            'active' => $this->boolean()

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190624_164929_new_api_access_table cannot be reverted.\n";

        return false;
    }
    */
}
