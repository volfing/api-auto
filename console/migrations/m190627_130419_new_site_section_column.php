<?php

use yii\db\Migration;

/**
 * Class m190627_130419_new_site_section_column
 */
class m190627_130419_new_site_section_column extends Migration
{
    private $tableName = "site_section";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, "is_active", $this->boolean()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, "is_active");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190627_130419_new_site_section_column cannot be reverted.\n";

        return false;
    }
    */
}
