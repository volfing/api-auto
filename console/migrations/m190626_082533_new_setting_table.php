<?php

use yii\db\Migration;

/**
 * Class m190626_082533_new_setting_table
 */
class m190626_082533_new_setting_table extends Migration
{
    private $tableName = "setting";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'type' => $this->string(100)->notNull(),
            'name' => $this->string(100)->notNull(),
            'value' => $this->string(999)

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190626_082533_new_setting_table cannot be reverted.\n";

        return false;
    }
    */
}
