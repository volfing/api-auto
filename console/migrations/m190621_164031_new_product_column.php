<?php

use yii\db\Migration;

/**
 * Class m190621_164031_new_product_column
 */
class m190621_164031_new_product_column extends Migration
{
    private $tableName = "product";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, "site_section", $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, "site_section");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190621_164031_new_product_column cannot be reverted.\n";

        return false;
    }
    */
}
