<?php

use yii\db\Migration;

/**
 * Class m190620_105551_new_product_table
 */
class m190620_105551_new_product_table extends Migration
{
    private $tableName = ["product", "price", "quantity"];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName[0], [
            'id' => $this->primaryKey(),
            'original_id' => $this->string(100)->notNull(),
            'name' => $this->string(300)->notNull(),
            'original_image' => $this->string(300),
            'oem' => $this->string(50),
            'manufacturer' => $this->string(100),
            'for_year' => $this->string(30),
            'country' => $this->string(100),
            'manufacturer_number' => $this->string(100),
            'article' => $this->string(100),
            'original_parent' => $this->string(100)

        ], $tableOptions);

        $this->createTable($this->tableName[1], [
            'id' => $this->primaryKey(),
            'original_id' => $this->string(100)->notNull(),
            'type' => $this->smallInteger(3)->notNull(),
            'product_id' => $this->integer()->notNull(),
            'currency' => $this->string(6)->notNull(),
            'value' => $this->double()->notNull()
        ], $tableOptions);

        $this->createTable($this->tableName[2], [
            'id' => $this->primaryKey(),
            'original_id' => $this->string(100)->notNull(),
            'name' => $this->string(100)->notNull(),
            'product_id' => $this->integer()->notNull(),
            'value' => $this->integer(10)->notNull()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName[0]);
        $this->dropTable($this->tableName[1]);
        $this->dropTable($this->tableName[2]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190620_105551_new_product_table cannot be reverted.\n";

        return false;
    }
    */
}
