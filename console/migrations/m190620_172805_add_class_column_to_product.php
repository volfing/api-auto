<?php

use yii\db\Migration;

/**
 * Class m190620_172805_add_class_column_to_product
 */
class m190620_172805_add_class_column_to_product extends Migration
{
    private $tableName = "product";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, "class_key", $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, "class_key");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190620_172805_add_class_column_to_product cannot be reverted.\n";

        return false;
    }
    */
}
