<?php

use yii\db\Migration;

/**
 * Class m190626_144400_new_setting_row
 */
class m190626_144400_new_setting_row extends Migration
{

    private $tableName = "setting";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert($this->tableName, ["title" => 'Склады, которые нужно исключить из выдачи в API', "type" => "multiple-select", "value" => "", "name" => "excluded_warehouses"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['name' => "excluded_warehouses"]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190626_144400_new_setting_row cannot be reverted.\n";

        return false;
    }
    */
}
