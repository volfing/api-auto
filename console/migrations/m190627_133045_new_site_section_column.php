<?php

use yii\db\Migration;

/**
 * Class m190627_133045_new_site_section_column
 */
class m190627_133045_new_site_section_column extends Migration
{
    private $tableName = "site_section";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, "section_connection", $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, "section_connection");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190627_133045_new_site_section_column cannot be reverted.\n";

        return false;
    }
    */
}
