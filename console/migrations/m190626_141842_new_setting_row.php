<?php

use yii\db\Migration;

/**
 * Class m190626_141842_new_setting_row
 */
class m190626_141842_new_setting_row extends Migration
{
    private $tableName = "setting";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert($this->tableName, ["title" => 'Наценка в процентах на оптовую цену поставщика', "type" => "number", "value" => 18, "name" => "markup"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ["name" => "markup"]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190626_141842_new_setting_row cannot be reverted.\n";

        return false;
    }
    */
}
