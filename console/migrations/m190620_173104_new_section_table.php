<?php

use yii\db\Migration;

/**
 * Class m190620_173104_new_section_table
 */
class m190620_173104_new_section_table extends Migration
{
    private $tableName = "section";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'original_id' => $this->string(100)->notNull(),
            'name' => $this->string(300)->notNull(),
            'original_parent' => $this->string(100),
            'class_key' => $this->string(50),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190620_173104_new_section_table cannot be reverted.\n";

        return false;
    }
    */
}
