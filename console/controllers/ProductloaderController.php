<?php


namespace console\controllers;


use common\models\Price;
use common\models\Product;
use common\models\Quantity;
use common\models\Section;
use common\modules\Ulwex\AutobodyParser\Autobodyparser;
use yii\console\Controller;
use yii\helpers\Console;

class ProductloaderController extends Controller
{
    public function actionIndex(){
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        ini_set("memory_limit", "1024M");

        $newProducts = 0;
        $updatedProducts = 0;
        $times = [
            "started" => time()
        ];

        $parser = new Autobodyparser("b8ce003430746624b6cae784250ef3fe");
        $parser->getProducts([
            "10629",//Hyundai solaris 2017
            "9805",//Hyundai solaris 2011
            "9534",//Hyundai elantra 2007
            "9593",//Kia ceed 2007-2011
            "9922",//Kia ceed 2012
            "9788",//Kia optima 2011
            "10072",//Kia rio 2011-2017
            "10638",//Kia rio 2017
            "9779",//Bmw f10 5 series 2010
            "10478",//BMW F15 x5 2013
            "10461",//BMW F20/F21 1 series 2011
            "9793",//BMW F25 x3 2011
            "9759",//BMW F30 3 series 2012
            "10659",//BMW F48 x1 2016
            "10653",//BMW G11/G12 7 series 2015
            "10654",//BMW G30/31 5 series 2016
            "9988",//Skoda octavia 2004
            "9637",//Skoda octavia 2008-2012
            "10476",//Skoda octavia 2013
            "9707",//Vw golf plus 2004
            "10328",//Vw golf v 10/2003
            "9676",//Vw golf vi 2009-2011
            "10463",//Vw golf vii 2012
            "10363",//Vw polo 8/2001-2009
            "9853",//Vw polo 9/2010
        ]);

        $parser->getCatList();

        $times["data_parsed"] = time();

        $listOfCats = $parser->getPreparedSections();
        $listOfProducts = $parser->getPreparedProducts();

        $times["data_prepared"] = time();

        $localCats = Section::find()->all();

        //Делаем массив с разделами у нас в БД ассоциативным
        foreach ($localCats as $cat){
            $localCats[$cat->original_id] = $cat;
        }

        $times["loaded_cats"] = time();

        $this->addCats($localCats, $listOfCats);

        $times["added_cats"] = time();

        unset($listOfCats);
        unset($localCats);

        $localProducts = Product::find()->all();
        $assocPrices = [];
        $assocQuantity = [];
        $prices = Price::find()->all();
        $quantity = Quantity::find()->all();

        //Формируем ассоциативный массив со всеми ценами у нас в БД
        foreach ($prices as $price){
           if(empty($assocPrices[$price->product_id])){
               $assocPrices[$price->product_id] = [];
           }

           $assocPrices[$price->product_id][$price->original_id] = $price;
        }

        unset($prices);

        //Формируем ассоциативный массив со всеми остатками у нас в БД
        foreach ($quantity as $item){
            if(empty($assocQuantity[$item->product_id])){
                $assocQuantity[$item->product_id] = [];
            }

            $assocQuantity[$item->product_id][$item->original_id] = $item;
        }

        unset($quantity);

        $assocLocalProducts = [];

        foreach ($localProducts as $product){
            $assocLocalProducts[$product->original_id] = $product;
        }

        unset($localProducts);

        $times["loaded_products"] = time();

       //Добавляем или обновляем товар
       foreach ($listOfProducts as $product){
           if(!isset($assocLocalProducts[$product->original_id])){
               if($this->addProduct($product)){
                   $newProducts++;
               }
           }else{
               if($this->updateProduct($product, $assocLocalProducts[$product->original_id], $assocPrices, $assocQuantity)){
                   $updatedProducts++;
               }

               unset($assocLocalProducts[$product->original_id]);
           }
       }

       //Обнуляем остатки для товаров, которых не было в выгрузке
       foreach ($assocLocalProducts as $product){
           if(empty($product)){
               continue;
           }

           foreach ($assocQuantity[$product->id] as $quantity){
               if($quantity->value == 0){
                   continue;
               }

               $quantity->value = 0;

               if($quantity->save()){
                   $this->log("Остаток товара с ID {$product->id} на складе \"{$quantity->name}\" обнулен", "success");
               }else{
                   $this->log("Произошла ошибка при обнулении остатков у товара {$product->id}", "error");
               }
           }
       }

       $times["added_products"] = time();

       $this->log("Потрачено времени на парсинг данных: " . ($times["data_parsed"] - $times["started"]) . "sec", "info");
       $this->log("Потрачено времени на обработку данных: " . ($times["data_prepared"] - $times["started"]) . "sec", "info");
       $this->log("Потрачено времени на загрузку разделов из БД: " . ($times["loaded_cats"] - $times["started"]) . "sec", "info");
       $this->log("Потрачено времени на обновление разделов: " . ($times["added_cats"] - $times["started"]) . "sec", "info");
       $this->log("Потрачено времени на загрузку товаров из БД: " . ($times["loaded_products"] - $times["started"]) . "sec", "info");
       $this->log("Потрачено времени на обновление товаров: " . ($times["added_products"] - $times["started"]) . "sec", "info");
       $this->log("Добавлено новых товаров: {$newProducts}", "info");
       $this->log("Обновлено товаров: {$updatedProducts}", "info");
    }

    /**
     * Обновление товаров
     * @var $product \common\modules\Ulwex\ParserTemplate\Product
     * @var $localProduct Product
     * @var $assocPrices Price[][]
     * @var $assocQuantity Quantity[][]
     */
    private function updateProduct($product, $localProduct, $assocPrices, $assocQuantity){
        $wasUpdated = false;

        //Проверка остатков
        foreach ($product->quantity as $quantity) {
            //Значит у этого товара не присвоены остатки с этого склада
            if(!isset($assocQuantity[$localProduct->id][$quantity->original_id])){
                $nQuantity = new Quantity();
                $nQuantity->original_id = $quantity->original_id;
                $nQuantity->value = $quantity->value;
                $nQuantity->name = $quantity->name;
                $nQuantity->product_id = $localProduct->id;

                if($nQuantity->save()){
                    $this->log("Добавлены остатки со склада \"{$nQuantity->name}\" для товара с ID {$localProduct->id}","success");
                }else{
                    $this->log("Произошла ошибка при добавлении остатков к товару с ID {$localProduct->id}", "error");
                }

                $wasUpdated = true;

                continue;
            }

            //Остатки для данного склада нужно обновить
            if($assocQuantity[$localProduct->id][$quantity->original_id]->value != $quantity->value){
                $assocQuantity[$localProduct->id][$quantity->original_id]->value = $quantity->value;

                if($assocQuantity[$localProduct->id][$quantity->original_id]->save()){
                    $this->log("Остатки со склада \"{$quantity->name}\" для товара с ID {$localProduct->id} были успешно обновлены", "success");
                }else{
                    $this->log("Произошла ошибка во время обновления остаток для товара с ID {$localProduct->id}", "error");
                }

                $wasUpdated = true;
            }
        }

        //Проверка цен
        foreach ($product->prices as $price) {
            //Значит у этого товара не присвоена эта цена
            if(!isset($assocPrices[$localProduct->id][$price->original_id])){
                $nPrice = new Price();
                $nPrice->value = $price->value;
                $nPrice->product_id = $localProduct->id;
                $nPrice->original_id = $price->original_id;
                $nPrice->currency = $price->currency;
                $nPrice->type = $price->type;

                if($nPrice->save()){
                    $this->log("Добалена новая цена для товара с ID {$localProduct->id}", "success");
                }else{
                    $this->log("Произошла ошибка при добавлении цены к товару с ID {$localProduct->id}", "error");
                }

                $wasUpdated = true;

                continue;
            }

            //Значение для данного типа цены нужно обновить
            if($assocPrices[$localProduct->id][$price->original_id]->value != $price->value){
                $assocPrices[$localProduct->id][$price->original_id]->value = $price->value;

                if($assocPrices[$localProduct->id][$price->original_id]->save()){
                    $this->log("Значение цены для товара с ID {$localProduct->id} было успешно обновлено", "success");
                }else{
                    $this->log("Произошла ошибка во время обновления цены для товара с ID {$localProduct->id}", "error");
                }

                $wasUpdated = true;
            }
        }

        return $wasUpdated;
    }

    /**
     * @var $product \common\modules\Ulwex\ParserTemplate\Product
     * Добавление товаров на сайт
     */
    private function addProduct($product){
        $localProduct = new Product();
        $localProduct->original_id = $product->original_id;
        $localProduct->original_parent = $product->original_parent;
        $localProduct->name = $product->name;
        $localProduct->class_key = $product->class_key;
        $localProduct->manufacturer_number = $product->manufacturer_number;
        $localProduct->manufacturer = $product->manufacturer;
        $localProduct->country = $product->country;
        $localProduct->for_year = $product->for_year;
        $localProduct->original_image = $product->original_image;
        $localProduct->oem = $product->oem;
        $localProduct->article = $product->article;

        if($localProduct->save()){
            $this->log("Товар \"{$localProduct->name}\" добавлен в базу", "success");

            /*
             * Добавляем цены к товару
             */
            foreach ($product->prices as $price) {
                $localPrice = new Price();
                $localPrice->original_id = $price->original_id;
                $localPrice->type = $price->type;
                $localPrice->currency = $price->currency;
                $localPrice->value = $price->value;
                $localPrice->product_id = $localProduct->id;

                if($localPrice->save()){
                    $this->log("Цена для товара с ID {$localProduct->id} успешно добавлен", "success");
                }else{
                    $this->log("Произошла ошибка при добавении цены к товару с ID {$localProduct->id}", "error");
                }
            }

            /*
             * Добавляем остатки к товару
             */
            foreach ($product->quantity as $quantity){
                $localQuantity = new Quantity();
                $localQuantity->product_id = $localProduct->id;
                $localQuantity->original_id = $quantity->original_id;
                $localQuantity->name = $quantity->name;
                $localQuantity->value = $quantity->value;

                if($localQuantity->save()){
                    $this->log("Остаток для товара с ID {$localProduct->id} успешно добавлена", "success");
                }else{
                    $this->log("Произошла ошибка при добавении остатка к товару с ID {$localProduct->id}", "error");
                }
            }

            return true;
        }else{
            $this->log("Произошла ошибка во время сохранения товара \"{$localProduct->name}\"", "error");

            return false;
        }
    }

    /*
     * Добавление новых разделов на сайт
     */
    private function addCats($localCats, $listOfCats){
        //Добавляем раздел от поставщика, если его нет у нас
        foreach ($listOfCats as $cat){
            if(isset($localCats[$cat->original_id])){
                continue;
            }

            $section = new Section();
            $section->original_id = $cat->original_id;
            $section->class_key = $cat->class_key;
            $section->original_parent = $cat->original_parent;
            $section->name = $cat->name;

            if($section->save()){
                $this->log("Добавлен раздел \"{$section->name}\"", "success");
            }else{
                $this->log("При добавлении раздела \"{$section->name}\" произошла ошибка", "error");
            }
        }
    }

    /**
     * Добавляет сообщение на вывод в консоль
     * @param $message string
     * @param string $type string
     */
    private function log($message, $type = "default"){
        $message .= "\n";
        $message = date("Y.m.d H:i:s") . ": " . $message;

        switch ($type){
            case "error":
                $this->stdout($message, Console::FG_RED);
                break;
            case "success":
                $this->stdout($message, Console::FG_GREEN);
                break;
            case "info":
                $this->stdout($message, Console::FG_YELLOW);
                break;
            default:
                $this->stdout($message);
        }
    }
}