<?php


namespace common\modules\Ulwex\AutobodyParser;


use common\modules\Ulwex\ParserTemplate\ParserTemplate;
use common\modules\Ulwex\ParserTemplate\Price;
use common\modules\Ulwex\ParserTemplate\Product;
use common\modules\Ulwex\ParserTemplate\Quantity;
use common\modules\Ulwex\ParserTemplate\Section;

class Autobodyparser extends ParserTemplate
{
    private $token = null;
    private $baseUrl = "http://www.autobody.ru/api/";
    private $originalCatalog = null;
    private $originalProducts = null;

    protected function prepareProducts()
    {
        if(empty($this->originalProducts) || empty($this->originalProducts["elements"])){
            return;
        }

        $this->products = [];

        foreach ($this->originalProducts["elements"] as $product){
            $elem = new Product();
            $elem->article = !empty($product["code"]) ? $product["code"] : "";
            $elem->name = mb_strtolower($product["name"]);
            $elem->name = mb_strtoupper(mb_substr($elem->name, 0, 1)) . mb_substr($elem->name, 1);
            $elem->original_id = $product["xml_id"];
            $elem->original_image = !empty($product["image"]) ? $product["image"] : "";
            $elem->original_parent = $product["section_XML_ID"];
            $elem->oem = !empty($product["properties"]["oem"]) ? $product["properties"]["oem"] : "";
            $elem->manufacturer = !empty($product["properties"]["firm"]) ? $product["properties"]["firm"] : "";
            $elem->for_year = !empty($product["properties"]["year"]) ? $product["properties"]["year"] : "";
            $elem->country = !empty($product["properties"]["made_in"]) ? $product["properties"]["made_in"] : "";
            $elem->manufacturer_number = !empty($product["properties"]["manufacturer_number"]) ? $product["properties"]["manufacturer_number"] : "";
            $elem->class_key = "autobody";

            $prices = [];

            foreach ($product["prices"] as $price){
                $priceElem = new Price();
                $priceElem->original_id = $price["id"];
                $priceElem->currency = $price["currency"];
                $priceElem->value = $price["price"];
                $priceElem->type = $price["name"] == "Оптовая" ? 1 : 2;
                $prices[] = $priceElem;
            }

            $quantity = [];

            foreach ($product["amount"] as $item) {
                $amountElem = new Quantity();
                $amountElem->original_id = $item["id"];
                $amountElem->name = $item["name"];
                $amountElem->value = $item["quantity"];
                $quantity[] = $amountElem;
            }

            $elem->prices = $prices;
            $elem->quantity = $quantity;

            $this->products[] = $elem;
        }
    }

    protected function prepareSections()
    {
        if(empty($this->originalCatalog) || empty($this->originalCatalog["sections"])){
            return;
        }

        $this->sections = [];

        $sectionIDs = [];

        foreach ($this->originalCatalog["sections"] as $section) {
            $sectionIDs[$section["sectionCode"]] = $section["section_XML_ID"];
        }

        foreach ($this->originalCatalog["sections"] as $section){
            $sectionElem = new Section();
            $sectionElem->name = mb_strtolower($section["name"]);
            $sectionElem->name = mb_strtoupper(mb_substr($sectionElem->name, 0, 1)) . mb_substr($sectionElem->name, 1);
            $sectionElem->original_id = $section["section_XML_ID"];
            $sectionElem->original_parent = !empty($section["parentCode"]) ? $sectionIDs[$section["parentCode"]] : "";
            $sectionElem->class_key = "autobody";
            $this->sections[] = $sectionElem;
        }
    }

    public function getPreparedSections(){
        $this->prepareSections();

        return $this->sections;
    }

    public function getPreparedProducts(){
        $this->prepareProducts();

        return $this->products;
    }

    public function __construct($token){
        $this->token = $token;
    }

    public function getCatList(){
        return $this->getCats();
    }

    public function getProducts($sectionCode = []){
        $res = [];

        if(count($sectionCode) > 1){
            foreach ($sectionCode as $value) {
                $res = array_merge($res, $this->getProductList($value));
            }
        }else{
            $res = $this->getProductList(!empty($sectionCode) ? $sectionCode[0] : null);
        }

        return $res;
    }

    public function clearData(){
        $this->originalProducts = [];
        $this->originalCatalog = [];
    }

    private function getProductList($sectionCode = null){
        $data = [];

        if(!empty($sectionCode)){
            $data["section_code"] = $sectionCode;
        }

        $products = $this->request("getCatalog", $data, true);

        $products = !empty($products["elements"]) ? $products["elements"] : [];

        if(!empty($this->originalProducts["elements"])){
            $this->originalProducts["elements"] = array_merge($this->originalProducts["elements"], $products);
        }else{
            $this->originalProducts = ["elements" => $products];
        }

        return $products;
    }

    private function getCats(){
        $cats = $this->request("getSections", [], true);

        $cats = !empty($cats["sections"]) ? $cats["sections"] : [];

        if(!empty($this->originalCatalog["sections"])){
            $this->originalCatalog["sections"] += $cats;
        }else{
            $this->originalCatalog = ["sections" => $cats];
        }

        return $cats;
    }

    private function request($method, $data =  array(), $json_decode = false){
        $data["token"] = $this->token;
        $url = $this->baseUrl . $method . "/?" . http_build_query($data);
        $response = file_get_contents($url);

        if(!empty($response) && $json_decode){
            $response = json_decode($response, true);
        }

        return $response;
    }
}