<?php


namespace common\modules\Ulwex\ParserTemplate;


class Product
{
    public $name;
    public $original_id;
    public $original_image;
    public $oem;
    public $manufacturer;
    public $for_year;
    public $country;
    public $manufacturer_number;
    public $article;
    public $original_parent;
    public $class_key;
    /**
     * @var Price[]
     */
    public $prices;
    /**
     * @var Quantity[]
     */
    public $quantity;
}