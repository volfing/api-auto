<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "site_section".
 *
 * @property int $id
 * @property string $name
 * @property string $parent
 * @property boolean $is_active
 * @property int $section_connection
 */
class SiteSection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_section';
    }

    public function beforeSave($insert){
        if($this->parent == -1){
            $this->parent = null;
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 300],
            [['parent'], 'string', 'max' => 100],
            [['is_active', 'section_connection'], 'integer']
        ];
    }

    public static function getDisabledIDS(){
        $disabled_sections = static::find()->select(["id"])->where(["is_active" => 0])->asArray()->all();

        $disabled = [];

        foreach ($disabled_sections as $section){
            $disabled = array_merge($disabled, static::getAllChildIDS($section["id"]), [(int)$section["id"]]);
        }

        return $disabled;
    }

    public static function getAllChildIDS($parent){
        $childIDS = [];

        $sections = static::find()->where(['parent' => $parent])->all();

        foreach ($sections as $section){
            $childIDS[] = $section->id;

            $childIDS = array_merge($childIDS, static::getAllChildIDS($section->id));
        }

        if(empty($childIDS)){
            $childIDS[] = $parent;
        }

        return array_unique($childIDS);
    }

    public static function getSectionsTree($parent = null){
        $sections = null;

        if(!empty($parent)){
            $sections = static::find()->where(['parent' => $parent])->orWhere(['id' => $parent])->asArray()->all();
        }else{
            $sections = static::find()->asArray()->all();
        }


        foreach ($sections as $key => $section){
            if(empty($section["parent"])){
                continue;
            }

            $parent = &static::getParentSection($section["parent"], $sections);

            if(!empty($parent)){
                if(empty($parent["childs"])){
                    $parent["childs"] = [];
                }

                $parent["childs"][] = $section;

                unset($sections[$key]);
            }
        }

        return $sections;
    }


    private static function &getParentSection($id, &$sections){
        foreach ($sections as &$section){
            if($section["id"] == $id){
                return $section;
            }

            if(!empty($section["childs"])){
                $parent = &static::getParentSection($id, $section["childs"]);

                if(!empty($parent)){
                    return $parent;
                }
            }
        }

        $a = [];

        return $a;
    }

    public static function getSectionListForSelect($sections = null, $level = 0){
        if(empty($sections)){
            $sections = static::getSectionsTree();
        }

        foreach ($sections as $section){
            $prefix = "";

            for($i = 0;$i < $level;$i ++){
                $prefix .= "-";
            }

            $options[] = [
                "name" => $prefix . $section["name"],
                "id" => $section["id"]
            ];

            if(!empty($section["childs"])){
                $options = array_merge($options, static::getSectionListForSelect($section["childs"], $level+1));
            }
        }

        return $options;
    }

    public function getConnectedProductsCount(){
        if(empty($this->section_connection)){
            return 0;
        }

        $originalID = Section::findOne($this->section_connection);

        if(empty($originalID)){
            return 0;
        }

        $originalID = $originalID->original_id;

        $products = Product::find()->select(['id'])->where(['original_parent' => $originalID, 'site_section' => null])->count();

        return $products;
    }

    public static function getFormatedListForMenu($sections){
        $html = "<ul>";

        foreach ($sections as $section){
            $html .= "<li data-id='{$section['id']}'>" . $section["name"] . "<i class='glyphicon glyphicon-plus'></i></li>";

            if(!empty($section["childs"])){
                $html .= static::getFormatedListForMenu($section["childs"]);
            }
        }

        return $html . "</ul>";
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID раздела',
            'name' => 'Название раздела',
            'parent' => 'Родительский раздел',
            'is_active' => 'Раздел активен',
            'section_connection' => 'Связь с разделом от поставщиков',
        ];
    }
}
