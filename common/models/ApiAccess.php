<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "api_access".
 *
 * @property int $id
 * @property string $api_key
 * @property string $title
 * @property int $active
 */
class ApiAccess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_access';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['active'], 'integer'],
            [['api_key', 'title'], 'string', 'max' => 100],
        ];
    }

    public function beforeSave($insert)
    {
        if(empty($this->api_key)){
            $this->api_key = md5(rand(0, 99999) . microtime());
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'api_key' => 'Ключ',
            'title' => 'Заголовок',
            'active' => 'Активен',
        ];
    }
}
