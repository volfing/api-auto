<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form of `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'site_section'], 'integer'],
            [['original_id', 'name', 'original_image', 'oem', 'manufacturer', 'for_year', 'country', 'manufacturer_number', 'article', 'original_parent', 'class_key'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $parents = [];

        if(!empty($this->original_parent)){
            $parents = Section::getAllChildIDS($this->original_parent);
        }

        $siteSectionParents = null;

        if(!empty($this->site_section)){
            $siteSectionParents = SiteSection::getAllChildIDS($this->site_section);
            $query->andFilterWhere(['site_section' => $siteSectionParents]);
        }else{
            $query->andFilterWhere(['site_section' => null]);
        }

        $query->andFilterWhere(['like', 'original_id', $this->original_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'original_image', $this->original_image])
            ->andFilterWhere(['like', 'oem', $this->oem])
            ->andFilterWhere(['like', 'manufacturer', $this->manufacturer])
            ->andFilterWhere(['like', 'for_year', $this->for_year])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'manufacturer_number', $this->manufacturer_number])
            ->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['original_parent' => $parents])
            ->andFilterWhere(['like', 'class_key', $this->class_key]);

        //var_dump($parents);
        //var_dump($siteSectionParents);exit;

        return $dataProvider;
    }
}
