<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $original_id
 * @property string $name
 * @property string $original_image
 * @property string $oem
 * @property string $manufacturer
 * @property string $for_year
 * @property string $country
 * @property string $manufacturer_number
 * @property string $article
 * @property string $original_parent
 * @property string $class_key
 * @property int $site_section
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_section'], 'integer'],
            [['original_id', 'name'], 'required'],
            [['original_id', 'manufacturer', 'country', 'manufacturer_number', 'article', 'original_parent'], 'string', 'max' => 100],
            [['name', 'original_image'], 'string', 'max' => 300],
            [['oem', 'class_key'], 'string', 'max' => 50],
            [['for_year'], 'string', 'max' => 30],
        ];
    }

    public function getOriginalParentName(){
        $section = Section::find()->where(["original_id" => $this->original_parent])->one();

        if(empty($section)){
            return "";
        }

        return $section->name;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID товара',
            'original_id' => 'ID товара у поставщика',
            'name' => 'Название',
            'original_image' => 'Фото товара',
            'oem' => 'OEM',
            'manufacturer' => 'Производитель',
            'for_year' => 'For Year',
            'country' => 'Страна производства',
            'manufacturer_number' => 'Номер товара от производителя',
            'article' => 'Артикул',
            'original_parent' => 'Original Parent',
            'class_key' => 'Class Key',
            'site_section' => 'ID родительского раздела для выгрузки',
        ];
    }
}
