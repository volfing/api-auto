<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property string $value
 * @property string $name
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'type', 'name'], 'required'],
            [['title', 'type', 'name'], 'string', 'max' => 100],
            [['value'], 'string', 'max' => 999],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'type' => 'Type',
            'value' => 'Value',
            'name' => 'Name',
        ];
    }

    public static function getValue($name){
        $setting = static::find()->where(['name' => $name])->one();

        if(empty($setting)){
            return null;
        }

        return $setting->value;
    }

    public static function saveSettings($data){
        $errors = [];

        foreach ($data as $name => $value){
            $setting = static::find()->where(['name' => $name])->one();

            if(empty($setting)){
                continue;
            }

            if($setting->type == "number"){
                $value = (float)$value;
                $value = (string)$value;
            }

            if($setting->type == "multiple-select"){
                $value = is_array($value) ? implode(",", $value) : $value;
            }

            $setting->value = $value;

            if(!$setting->save()){
                $errors[$setting->name] = "Произошла ошибка во время сохранения настройки '{$setting->title}'";
            }
        }

        return $errors;
    }
}
