<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property int $id
 * @property string $original_id
 * @property string $name
 * @property string $original_parent
 * @property string $class_key
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['original_id', 'name'], 'required'],
            [['original_id', 'original_parent'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 300],
            [['class_key'], 'string', 'max' => 50],
        ];
    }

    public static function getAllChildIDS($original_parent){
        $childIDS = [];

        $sections = static::find()->where(['original_parent' => $original_parent])->all();

        foreach ($sections as $section){
            $childIDS[] = $section->original_id;

            $childIDS = array_merge($childIDS, static::getAllChildIDS($section->original_id));
        }

        if(empty($childIDS)){
            $childIDS[] = $original_parent;
        }

        return $childIDS;
    }

    public static function getSectionsList(){
        $sections = static::find()->all();

        $assocSections = [];

        foreach ($sections as $section){
            $assocSections[$section->id] = $section->name;
        }

        unset($sections);

        return $assocSections;
    }

    public static function getSectionsTree(){
        $sections = static::find()->asArray()->all();

        foreach ($sections as $key => $section){
            if(empty($section["original_parent"])){
                continue;
            }

            $parent = &static::getParentSection($section["original_parent"], $sections);

            if(!empty($parent)){
                if(empty($parent["childs"])){
                    $parent["childs"] = [];
                }

                $parent["childs"][] = $section;

                unset($sections[$key]);
            }
        }

        return $sections;
    }


    private static function &getParentSection($id, &$sections){
        foreach ($sections as &$section){
            if($section["original_id"] == $id){
                return $section;
            }

            if(!empty($section["childs"])){
                $parent = &static::getParentSection($id, $section["childs"]);

                if(!empty($parent)){
                    return $parent;
                }
            }
        }

        $a = [];

        return $a;
    }

    public static function getFormatedListForMenu($sections){
        $html = "<ul>";

        foreach ($sections as $section){
            $html .= "<li data-id='{$section['original_id']}'>" . $section["name"] . " [{$section['id']}] <i class='glyphicon glyphicon-plus'></i></li>";

            if(!empty($section["childs"])){
                $html .= static::getFormatedListForMenu($section["childs"]);
            }
        }

        return $html . "</ul>";
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_id' => 'Original ID',
            'name' => 'Name',
            'original_parent' => 'Original Parent',
            'class_key' => 'Class Key',
        ];
    }
}
