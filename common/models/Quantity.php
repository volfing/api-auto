<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "quantity".
 *
 * @property int $id
 * @property string $original_id
 * @property string $name
 * @property int $product_id
 * @property int $value
 */
class Quantity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quantity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['original_id', 'name', 'product_id', 'value'], 'required'],
            [['product_id', 'value'], 'integer'],
            [['original_id', 'name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_id' => 'Original ID',
            'name' => 'Name',
            'product_id' => 'Product ID',
            'value' => 'Value',
        ];
    }
}
