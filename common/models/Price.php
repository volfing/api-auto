<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "price".
 *
 * @property int $id
 * @property string $original_id
 * @property int $type
 * @property int $product_id
 * @property string $currency
 * @property double $value
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['original_id', 'type', 'product_id', 'currency', 'value'], 'required'],
            [['type', 'product_id'], 'integer'],
            [['value'], 'number'],
            [['original_id'], 'string', 'max' => 100],
            [['currency'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_id' => 'Original ID',
            'type' => 'Type',
            'product_id' => 'Product ID',
            'currency' => 'Currency',
            'value' => 'Value',
        ];
    }
}
