<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/jquery-ui-1.9.2.custom.min.css',
        'css/bootstrap-select.min.css',
        'css/custom.css',
    ];
    public $js = [
        'js/bootstrap-notify.min.js',
        'js/bootstrap-select.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
