<?php


namespace backend\controllers;


use common\models\Quantity;
use common\models\Setting;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $errors = [];

        $saved = false;

        $request = \Yii::$app->request;

        if(!empty($request->post("save_data"))){
            $errors = Setting::saveSettings($request->post());

            if(empty($errors)){
                $saved = true;
            }
        }

        $warehouses = Quantity::find()->select(['original_id as value', 'name as title'])->distinct()->asArray()->all();

        $settings = Setting::find()->all();

        return $this->render("index", [
            "settings" => $settings,
            "errors" => $errors,
            "saved" => $saved,
            "warehouses" => $warehouses
        ]);
    }
}