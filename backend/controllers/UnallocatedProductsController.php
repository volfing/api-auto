<?php


namespace backend\controllers;


use common\models\ProductSearch;
use common\models\Section;
use common\models\SiteSection;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class UnallocatedProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($section){
        $section = SiteSection::findOne((int)$section);

        if(empty($section)){
            return "Указан некорректный ID раздела";
        }

        if(empty($section->section_connection)){
            return "У указанного раздела отсутствует связь с разделом от поставщиков";
        }

        $originalSection = Section::findOne($section->section_connection);

        if(empty($originalSection)){
            return "Указана некорректная связь раздела";
        }

        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['original_parent' => $originalSection->original_id])->andWhere(['site_section' => null]);

        $sectionsTree = SiteSection::getSectionsTree($section->id);

        $siteSections = SiteSection::getSectionListForSelect();

        return $this->render('index', [
            "dataProvider" => $dataProvider,
            "searchModel" => $searchModel,
            "sectionsTree" => $sectionsTree,
            "siteSections" => $siteSections
        ]);
    }

}