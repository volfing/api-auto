<?php

namespace backend\controllers;

use common\models\Section;
use common\models\SiteSection;
use Yii;
use common\models\Product;
use common\models\ProductSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SiteProductController implements the CRUD actions for Product model.
 */
class SiteProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'change-parent', 'link-products'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $catName = "";

        $currentSectionID = 0;

        if(!empty($_REQUEST["ProductSearch"]["site_section"])){
            $cat = SiteSection::find()->where(['id' => $_REQUEST["ProductSearch"]["site_section"]])->one();

            if(!empty($cat)){
                $currentSectionID = $cat->id;
                $catName = $cat->name;
            }
        }else{
            $dataProvider->query->andWhere(['>', 'site_section', 0]);
        }

        $siteSections = SiteSection::getSectionListForSelect();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'catName' => $catName,
            'siteSections' => $siteSections,
            'currentSectionID' => $currentSectionID,
        ]);
    }

    public function actionChangeParent($productID, $newParentID){
        Yii::$app->response->format = Response::FORMAT_JSON;

        $product = Product::findOne((int)$productID);

        if(empty($product)){
            return ["success" => false, "message" => "Указан некорректный ID товара"];
        }

        $section = SiteSection::findOne($newParentID);

        if(empty($section)){
            return ["success" => false, "message" => "Указан некорректный ID нового раздела"];
        }

        $product->site_section = $section->id;

        if($product->save()){
            return ["success" => true];
        }else{
            return ["success" => false, "message" => "Произошла ошибка во время сохранения данных"];
        }
    }

    public function actionLinkProducts(){
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(empty($_POST["ids"])){
            return ['success' => false, 'message' => 'Не указаны ID товаров для перемещения'];
        }

        if(empty($_POST['toSection'])){
            return ['success' => false, 'message' => 'Не указан ID нового раздела'];
        }

        $section = SiteSection::findOne((int)$_POST['toSection']);

        if(empty($section)){
            return ['success' => false, 'message' => 'Указан некорректный ID нового раздела'];
        }

        Product::updateAll(['site_section' => $section->id], 'id IN (' . $_POST['ids'] . ')');

        return ['success' => true, "count" => count(explode(",", $_POST['ids'])), "category_name" => $section->name];
    }
}
