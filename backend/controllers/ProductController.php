<?php

namespace backend\controllers;

use common\models\Section;
use common\models\SiteSection;
use Yii;
use common\models\Product;
use common\models\ProductSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'link-products'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionLinkProducts(){
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(empty($_POST["toSection"])){
            return ['success' => false, 'message' => 'Не указан раздел для привязки товаров'];
        }

        $siteSection = SiteSection::findOne((int)$_POST['toSection']);

        if(empty($siteSection)){
            return ['success' => false, 'message' => 'Указанного раздела не существует'];
        }

        $products = [];

        if(!empty($_POST['fromSection'])){
            $section = Section::find()->where(['original_id' => (int)$_POST['fromSection']])->one();

            if(empty($section)){
                return ['success' => false, 'message' => 'Раздела (из которого нужно выбрать товары) не существует'];
            }

            $parents = Section::getAllChildIDS($section->original_id);

            //var_dump($parents);exit;

            $childs = Product::find()->where(['original_parent' => $parents])->andWhere(['site_section' => null])->all();

            foreach ($childs as $child){
                $child->site_section = $siteSection->id;
                $child->save();
            }

            return ['success' => true, 'count' => count($childs), 'category_name' => $section->name];
        }

        return ['success' => false, 'message' => 'Произошла непредвиденная ошибка'];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(["site_section" => null]);

        $catName = "";

        $currentSectionID = 0;

        if(!empty($_REQUEST["ProductSearch"]["original_parent"])){
            $cat = Section::find()->where(['original_id' => $_REQUEST["ProductSearch"]["original_parent"]])->one();

            if(!empty($cat)){
                $currentSectionID = $cat->original_id;
                $catName = $cat->name;
            }
        }

        $siteSections = SiteSection::getSectionListForSelect();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'catName' => $catName,
            'siteSections' => $siteSections,
            'currentSectionID' => $currentSectionID,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        $sectionsTree = SiteSection::getSectionsTree();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'sectionsTree' => $sectionsTree,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $sectionsTree = SiteSection::getSectionsTree();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'sectionsTree' => $sectionsTree,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
