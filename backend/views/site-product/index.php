<?php

use common\models\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $catName string */
/* @var $siteSections [] */
/* @var $currentSectionID int */

$this->registerJsFile("js/jquery-2.2.4.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("js/jquery-ui-1.9.2.custom.min.js", ['position' => View::POS_HEAD]);

$this->title = 'Список товаров для выгрузки на сайт';

$this->params['breadcrumbs'][] = ["label" => $this->title, "url" => \yii\helpers\Url::to(['site-product/index'])];

if(!empty($catName)){
    $this->title .= " - " . $catName;
    $this->params['breadcrumbs'][] = $catName;
}

?>

<script>
    var jq2 = jQuery.noConflict();
</script>

<div class="product-index container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-3">
            <?echo $this->render("_search", ['model' => $searchModel])?>
        </div>
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'id'
                    ],
                    'id',
                    'name',
                    'manufacturer_number',
                    'article',
                    //'class_key',
                    //'site_section',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>

        <div class="col-md-12 mb-30">
            <div style="float:right;" class="btn btn-success" data-toggle="modal" data-target="#link_products">Перенести выбранные товары в другой раздел</div>
        </div>
    </div>

</div>

<?= $this->render('/modals/link-product', ['siteSections' => $siteSections]) ?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        initDrangDropProducts('<?=\yii\helpers\Url::to(['site-product/change-parent'])?>');
    });
</script>
