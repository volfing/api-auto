<?php

use common\models\ProductSearch;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?
    $sections = \common\models\SiteSection::getSectionsTree();
    $sectionList = \common\models\SiteSection::getFormatedListForMenu($sections);

    ?>

    <h4>Категория</h4>
    <div class="list_of_cats">
        <?=$sectionList?>
    </div>

    <?php  echo $form->field($model, 'site_section')->hiddenInput()->label(false) ?>

    <?php ActiveForm::end(); ?>

</div>
