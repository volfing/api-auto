<?php

use yii\grid\GridView;
use yii\web\View;

/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel \common\models\ProductSearch
 * @var $sectionsTree []
 * @var $siteSections []
 */

$this->registerJsFile("js/jquery-2.2.4.min.js", ['position' => View::POS_HEAD]);
$this->registerJsFile("js/jquery-ui-1.9.2.custom.min.js", ['position' => View::POS_HEAD]);

$this->title = "Распредление товаров";

?>

<script>
    var jq2 = jQuery.noConflict();
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1><?=$this->title?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?
            $sectionList = \common\models\SiteSection::getFormatedListForMenu($sectionsTree);

            ?>

            <h4>Категория</h4>
            <div class="list_of_cats">
                <?=$sectionList?>
            </div>
        </div>
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'name' => 'id'
                    ],
                    'id',
                    'name',
                    'oem',
                    'manufacturer',
                    'manufacturer_number',
                    'article',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>

        <div class="col-md-12 mb-30">
            <div style="float:right;" class="btn btn-success" data-toggle="modal" data-target="#link_products">Перенести выбранные товары в другой раздел</div>
        </div>
    </div>
</div>

<?= $this->render('/modals/link-product', ['siteSections' => $siteSections]) ?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        initDrangDropProducts('<?=\yii\helpers\Url::to(['site-product/change-parent'])?>');
    });
</script>