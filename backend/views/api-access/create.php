<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApiAccess */

$this->title = 'Создание API ключа';
$this->params['breadcrumbs'][] = ['label' => 'API ключи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-access-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
