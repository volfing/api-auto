<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApiAccess */

$this->title = 'Изменение API ключа: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'API ключи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="api-access-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
