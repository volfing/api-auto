<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSection */
/* @var $sectionsTree [] */

$this->title = 'Создать раздел';
$this->params['breadcrumbs'][] = ['label' => 'Разделы для выгрузки на сайт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-section-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sectionsTree' => $sectionsTree,
    ]) ?>

</div>
