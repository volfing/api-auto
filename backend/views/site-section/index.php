<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SiteSectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы для выгрузки на сайт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-section-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать раздел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'parent',
            [
                'label' => "Кол-во нераспределенных товаров",
                'content' => function($data){
                    $count = $data->getConnectedProductsCount();

                    $html = $count;

                    if($count > 0){
                        $html = "<span style='color:green;font-weight: bold;margin-right: 10px;'>+{$count}</span><a href='" . \yii\helpers\Url::to(['unallocated-products/index', 'section' => $data->id]) . "'>перейти к распределению</a>";
                    }

                    return $html;
                }
            ],
            [
                'attribute' => 'is_active',
                'content' => function($data){
                    return $data->is_active == 1 ? "Да" : "<span style='color: red;'>Нет</span>";
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
