<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSection */
/* @var $form yii\widgets\ActiveForm */
/* @var $sectionsTree [] */
?>

<div class="site-section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?
    $options = ["-1" => ""] + getChildSections($sectionsTree);
    ?>

    <?= $form->field($model, 'parent')->dropDownList($options) ?>

    <?= $form->field($model, 'section_connection')->textInput() ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?
function getChildSections($sections, $level = 0){
    $options = [];

    foreach ($sections as $section){
        $prefix = "";

        for($i = 0;$i < $level;$i ++){
            $prefix .= "-";
        }

        $options[$section["id"]] = $prefix . $section["name"];

        if(!empty($section["childs"])){
            $options = $options + getChildSections($section["childs"], $level+1);
        }
    }

    return $options;
}
?>