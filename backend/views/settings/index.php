<?php

/**
 * @var $settings \common\models\Setting[]
 * @var $errors []
 * @var $saved boolean
 * @var $warehouses []
 */

$this->title = "Настройки";

use yii\helpers\Html; ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1><?=$this->title?></h1>
        </div>

        <div class="col-md-12">
            <?foreach ($errors as $error):?>
                <div class="alert alert-danger" role="alert">
                    <?=$error?>
                </div>
            <?endforeach;?>

            <?if(!empty($saved)):?>
                <div class="alert alert-success" role="alert">
                   Настройки были успешно сохранены
                </div>
            <?endif;?>

            <?$form = \yii\bootstrap\ActiveForm::begin();?>

            <div class="form-group">
                <?foreach ($settings as $setting):?>
                    <label style="margin-top: 10px;" class="control-label" for="setting-<?=$setting->name?>"><?=$setting->title?></label>
                    <?if($setting->type == "number"):?>
                        <input type="number" id="setting-<?=$setting->name?>" class="form-control" name="<?=$setting->name?>" value="<?=$setting->value?>">
                    <?endif;?>

                    <?if($setting->type == "multiple-select"):?>
                        <?
                        $selectData = [];

                        if($setting->name == "excluded_warehouses"){
                            $selectData = $warehouses;
                        }

                        $setting->value = explode(",", $setting->value);
                        ?>

                        <select id="setting-<?=$setting->name?>" name="<?=$setting->name?>[]" class="form-control selectpicker" multiple>
                            <?foreach ($selectData as $item):?>
                                <option value="<?=$item['value']?>" <?=(in_array($item['value'], $setting->value) ? 'selected' : '')?>><?=$item['title']?></option>
                            <?endforeach;?>
                        </select>
                    <?endif;?>
                <?endforeach;?>
            </div>

            <input type="hidden" name="save_data" value="1">

            <div class="form-group">
                <?= Html::submitButton('Сохранить настройки', ['class' => 'btn btn-success']) ?>
            </div>

            <? \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>