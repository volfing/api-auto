<?php
/**
 * @var $siteSections []
 */
?>

<div class="modal" tabindex="-1" role="dialog" id="link_products">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Привязка товаров  разделам</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <select class="custom-select" name="section">
                    <option value="-1" selected>Выберите раздел, к которому нужно привязать товары</option>
                    <?foreach ($siteSections as $section):?>
                        <option value="<?=$section['id']?>"><?=$section['name']?></option>
                    <?endforeach;?>
                </select>
            </div>
            <div class="modal-footer">
                <button id="save_products_link" type="button" class="btn btn-primary" onclick='productsLink("[name=\"id[]\"]", "#link_products [name=section]", "<?=\yii\helpers\Url::to(['site-product/link-products'])?>");return false;'>Привязать товары</button>
                <button type="button" class="btn btn-secondary"  data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
