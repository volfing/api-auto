<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 0
        ],
    ]); ?>

    <?
    $sections = \common\models\Section::getSectionsTree();
    $sectionList = \common\models\Section::getFormatedListForMenu($sections);
    ?>

    <h4>Категория</h4>
    <div class="list_of_cats">
        <?=$sectionList?>
    </div>

    <?php  echo $form->field($model, 'original_parent')->hiddenInput()->label(false) ?>

    <?php ActiveForm::end(); ?>

</div>
