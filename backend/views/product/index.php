<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $catName string */
/* @var $siteSections [] */
/* @var $currentSectionID int */

$this->title = 'Список товаров';

$this->params['breadcrumbs'][] = ["label" => $this->title, "url" => \yii\helpers\Url::to(['product/index'])];

if(!empty($catName)){
    $this->title .= " - " . $catName;
    $this->params['breadcrumbs'][] = $catName;
}

?>
<div class="product-index container">

    <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-md-12">
                <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
            <div class="col-md-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        'name',
                        'oem',
                        'manufacturer',
                        //'for_year',
                        //'country',
                        'manufacturer_number',
                        'article',
                        //'class_key',
                        //'site_section',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>

            <?if(!empty($currentSectionID)):?>
                <div class="col-md-12 mb-30">
                    <div class="btn btn-success" data-toggle="modal" data-target="#link_products">Привязать все товары из этого раздела к разделу на сайте</div>
                </div>
            <?endif;?>
        </div>


</div>

<?= $this->render('/modals/link-product', ['siteSections' => $siteSections]) ?>
