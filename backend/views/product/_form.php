<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $sectionsTree [] */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'original_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'original_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oem')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'for_year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manufacturer_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'original_parent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'class_key')->textInput(['maxlength' => true]) ?>

    <?
    $options = ["-1" => ""] + getChildSections($sectionsTree);
    ?>

    <?= $form->field($model, 'site_section')->dropDownList($options) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?
function getChildSections($sections, $level = 0){
    $options = [];

    foreach ($sections as $section){
        $prefix = "";

        for($i = 0;$i < $level;$i ++){
            $prefix .= "-";
        }

        $options[$section["id"]] = $prefix . $section["name"];

        if(!empty($section["childs"])){
            $options = array_merge($options, getChildSections($section["childs"], $level+1));
        }
    }

    return $options;
}
?>
