document.addEventListener("DOMContentLoaded", function () {
    $("body").on('click', '.list_of_cats ul li i', function(e){
        e.preventDefault();

        $(this).parent().toggleClass("active");

        if($(this).parent().hasClass("active")){
            $(this).removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }else{
            $(this).removeClass("glyphicon-minus").addClass("glyphicon-plus");
        }
    });

    $("body").on('click', '.list_of_cats ul li', function(e){
        if(e.target != this || (!$("#productsearch-site_section").length && !$("#productsearch-original_parent").length)) {
            return;
        }

        var id = $(this).attr("data-id");

        if($("#productsearch-original_parent").length){
            $("#productsearch-original_parent").val(id).trigger("change");
            $("#productsearch-original_parent").closest("form").submit();
        }else{
            $("#productsearch-site_section").val(id).trigger("change");
            $("#productsearch-site_section").closest("form").submit();
        }



        $("h1").text($(this).text());
    });
});

function notify(message, type){
    $.notify({message: message}, {type: type});
}

/**
 * @var idsSelector string
 * @var selectSelector string
 * @var url string
Привязка товаров к разделу
 */
function productsLink(idsSelector, selectSelector, url){
    var ids = [];

    $(idsSelector).each(function(){
        if($(this).prop("checked")){
            ids[ids.length] = $(this).val();
        }
    });

    var value = $(selectSelector).val();

    if(parseInt(value) < 1){
        notify("Необходимо выбрать раздел", "danger");
        return;
    }

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
            ids: ids.join(","),
            toSection: value
        },
        success: function (data) {
            $(".modal.in").modal("hide");

            if(!data.success){
                notify(data.message, "danger");
                return;
            }else{
                notify("Товары (" + data.count + " шт) были успешно привязаны к разделу " + data.category_name, "success");
            }
        }
    });
}

function initDrangDropProducts(url){
    jq2(".list_of_cats ul li").droppable({
        drop: function(event, ui){
            var productID = $(ui.draggable[0]).attr("data-key");
            var newParentID = $(this).attr("data-id");

            $(this).addClass("dropped");

            var elem = this;

            setTimeout(function(){
                $(elem).removeClass("dropped");
            }, 400);

            $.ajax({
                type: "GET",
                url:  url,
                data: {
                    productID: productID,
                    newParentID: newParentID
                },
                success: function (data) {
                    if(!data.success){
                        notify(data.message, "danger");
                    }else{
                        notify("Товар успешно перемещен", "success");
                        $(ui.draggable[0]).remove();
                    }
                }
            });
        },
        activate: function() {
            $(this).css({
                backgroundColor: "#37de7b"
            });
        },
        hoverClass: "hover",
        deactivate: function() {
            $(this).css("background-color", "");
        }
    });

    jq2(".table > tbody > tr").draggable({
        helper:'clone',
        opacity: 0.6
    });
}