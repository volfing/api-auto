<?php


namespace frontend\controllers;


use common\models\ApiAccess;
use common\models\Product;
use common\models\Price;
use common\models\Quantity;
use common\models\Setting;
use common\models\SiteSection;
use yii\rest\Controller;
use yii\web\Response;

class ApiController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if(empty($_REQUEST["token"])){
            return false;
        }

        $token = ApiAccess::find()->where(['api_key' => $_REQUEST["token"], "active" => 1])->one();

        if(empty($token)){
            return;
        }

        return parent::beforeAction($action);
    }

    public function actionGetSections(){
        $sections = SiteSection::find()->where(['is_active' => 1])->asArray()->all();

        return ['sections' => $sections];
    }

    public function actionGetProducts(){
        $disabled_sections = SiteSection::getDisabledIDS();
        $products = Product::find()->where(['>', 'site_section', 0])->andWhere(['not in', 'site_section', $disabled_sections])->groupBy(['original_id'])->asArray()->all();
        $prices = Price::find()->where(['type' => 1])->asArray()->all();//Выбираем только оптовые цены
        $quantity = Quantity::find()->asArray()->all();

        $assocPrices = [];

        $markup = Setting::getValue("markup");//Наценка из настроек

        foreach ($prices as $price) {
            $productMarkup = $price["value"]/100 * $markup;//Считаем наценку

            $price["value"] = $price["value"] + $productMarkup;

            $assocPrices[$price["product_id"]] = $price;
        }

        unset($prices);

        $assocQuantity = [];

        $excludedWarehouses = Setting::getValue("excluded_warehouses");//Склады, которые нужно исключить ввыгрузке
        $excludedWarehouses = explode(",", $excludedWarehouses);

        $productsWithQuantity = [];

        foreach ($quantity as $item) {
            if(in_array($item["original_id"], $excludedWarehouses)){
                continue;
            }

            if(empty($assocQuantity[$item["product_id"]])){
                $assocQuantity[$item["product_id"]] = [];
            }

            $assocQuantity[$item["product_id"]][] = $item;

            if($item["value"] > 0){
                $productsWithQuantity[$item["product_id"]] = 1;
            }
        }

        //return count($productsWithQuantity);

        unset($quantity);

        foreach ($products as &$product) {
            $product["price"] = $assocPrices[$product["id"]];
            $product["quantity"] = $assocQuantity[$product["id"]];
        }

        return ['products' => $products];
    }
}